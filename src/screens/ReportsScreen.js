import React from 'react';
import { Content, Container, Text, Card, CardItem, Body, Icon, FooterTab, Button} from "native-base";
import API from '../api/initalize';
class ReportsScreen extends React.Component{
    // Global variables nimo ning naa sa state.
    constructor(props){
        super(props);
        this.state = {
            loading : false
        }
    }


  render(){
    let result;
    if(this.state.loading){
      result = <Container><Content contentContainerStyle={{justifyContent : 'center', alignContent : 'center'}}><Text>Loading data...</Text></Content></Container>
    }else{
      result = (
        <Content>
        <Card>
            <CardItem header style={{backgroundColor: "#ffc400", flex : 1, flexDirection : 'row', borderWidth : 0.50, borderColor : '#000000', elevation : 4}}>
                <Text header style={{fontSize: 20, color: "#000000"}}>{this.props.name}</Text>
            </CardItem>
            <CardItem bordered style={{backgroundColor: "#ffc400", borderColor : '#000000', borderWidth : 0.50, elevation : 4}}>
                <Text style={{color: "#000000"}}>{"Status: "}{this.props.crowd? 'Crowded':'Fine'}{"\n"}</Text>
            </CardItem>
            <CardItem bordered>
                <Body style={{flex: 1, flexDirection: "row"}}>
                    <Icon type="Feather" name='navigation' style={{ color: "#ffc400", paddingRight : 3}} />
                    <Text>{this.props.distance} kilometers{'\n'}<Text note>away from the DSA.</Text></Text>
                </Body>
            </CardItem>
            <CardItem bordered>
                <Body style={{flex: 1, flexDirection: "row"}}>
                    <Icon type="Feather" name='clock' style={{ color: "#ffc400", paddingRight : 3}} />
                    <Text>{this.props.time} minutes{'\n'}<Text note>away from the DSA.</Text></Text>
                </Body>
            </CardItem>
        </Card> 
        <Card>
            <CardItem header bordered style={{backgroundColor: "white"}}>
                <Body style={{flex: 1, flexDirection: "row",  alignContent :'flex-start'}}>
                    <Icon type="Feather" name='info' style={{ color: "#ffc400", paddingRight : 3}} /> 
                    <Text header style={{fontSize: 20, color: "#000000", paddingTop : 5}}>Additional Information</Text>
                </Body>
            </CardItem>
            <CardItem bordered>
                <Body>
                    <Text>Smoke: {this.props.smoke} PPM</Text>
                    <Text>Carbon Monoxide: {this.props.co} PPM</Text>
                </Body>
            </CardItem>
        </Card>
    </Content>
      )
    }
      return(
        result
      )
  }
}

export default ReportsScreen;