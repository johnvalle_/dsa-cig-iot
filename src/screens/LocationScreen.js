import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Constants, MapView } from 'expo';
import Loader from "../components/Loader";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

class LocationScreen extends Component {
  _isMounted = false;

  static navigationOptions =({navigation}) => {
    return{
      title: 'DSA-Cig-IoT',
      headerTitleStyle:{
          alignSelf:'center',
          textAlign: 'center',
          width: wp('65%'),
      },
      headerStyle: {
          backgroundColor: navigation.getParam('color'),
      },
      headerTintColor: '#FFFFFF',
    }
  };
  state = {
    userMarker : {
      title : "It's you, user.",
      description : 'Your current location.'
    },
    deviceMarker : {
      title : "DSA",
      description : 'The DSA is located here.'
    },
    errorMessage: null,
    isMapReady : false,
    coordinates : null,
    color : 'red'
  };

  onMapLayout = () => {
    this.setState({
      isMapReady : true
    })
  }
  componentDidMount() {
    const { navigation } = this.props;
    if( navigation.getParam('color') != "#ff9933" &&  navigation.getParam('color') != "#d9593b" &&  navigation.getParam('color') != "#a52a2a" ){
      this.props.navigation.setParams({ color: '#EAA936' });
    }else{
      this.props.navigation.setParams({ color:  navigation.getParam('color') });
    }
  }
  render() {
    const { navigation } = this.props;
    const userLoc = navigation.getParam('userLoc', '');
    const deviceLoc = navigation.getParam('deviceLoc', '');
    const coordinates = navigation.getParam('coordinates', '');

    let result;
    if(this.state.loading){
      result = <Loader />
    }else{
      result = (
         
          <View style={{flex : 1}}>
            <MapView
              style={styles.map}
              initialRegion={{
                latitude: userLoc.latitude,
                longitude: userLoc.longitude,
                latitudeDelta: 0.0243,
                longitudeDelta: 0.0234,
              }}
              onLayout={this.onMapLayout}
            >
              {
              this.state.isMapReady && 
                <MapView.Marker
                  coordinate={userLoc}
                  title={this.state.userMarker.title}
                  description={this.state.userMarker.description}
                />
              } 
              {
              this.state.isMapReady && 
                <MapView.Marker
                  coordinate={deviceLoc}
                  title={this.state.deviceMarker.title}
                  description={this.state.deviceMarker.description}
                  pinColor={'green'}
                />
              }
              {
                coordinates &&
                <MapView.Polyline
                  coordinates = {coordinates}
                  strokeWidth = {4}
                  strokeColor = {navigation.getParam('color')}
                />
              }
            </MapView>
          </View>
        )
      
    }
    return (
      result
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
  map :{
    ...StyleSheet.absoluteFillObject
  }
});
 
export default LocationScreen;

