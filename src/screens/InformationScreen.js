import React from 'react';
import { Content, Container, Text, Fab, Button, Icon } from "native-base";
import { Platform, AsyncStorage, ScrollView, RefreshControl, Switch } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Constants } from 'expo';
import API from '../api/initalize';
import Loader from '../components/Loader';
import LocationMap from '../components/LocationMap';
import Polyline from '@mapbox/polyline';
import VehicleTab from '../components/VehicleTab';
import ReportsScreen from './ReportsScreen';
import { ConfirmDialog } from 'react-native-simple-dialogs';


class InformationScreen extends React.Component{
    _isMounted = false;

    static navigationOptions = ({ navigation }) => ({
        title: 'DSA-Cig-IoT',
        headerLeft: null,
        headerBackTitle: null,
        headerTitleStyle:{
            alignSelf:'center',
            textAlign: 'center',
            width: wp('90%'),
        },
        headerStyle: {
            backgroundColor: '#ffc400',
        },
        headerTintColor: '#000000',
    });
 
    constructor(props){
        super(props);
        this.state = {
            token : '',
            userLocation : {
                latitude : 0.0,
                longitude : 0.0
            },
            lastValueCO : 0,
            lastValueSmoke : 0,
            // context1 : {
            //     latitude : 0.0,
            //     longitude : 0.0
            // },
            // context2 : {
            //     latitude : 0.0,
            //     longitude : 0.0
            // },
            context1 : {
                latitude : 7.091326,
                longitude : 125.613031
            },
            context2 : {
                latitude : 7.098981,
                longitude : 125.624183
            },
            loading : true,
            lineCoordinates : null,
            nearest : {
                latitude : 0.0,
                longitude : 0.0,
            },
            time : 0,
            name : "",
            crowd : 0,
            isCrowded : false,
            isOverCrowded : false,
            buttCount : {
              min : 0,
              max : 0
            },
            mapLoading : false,
            refreshing: false,
            tab : 1,
            active : false,
            selectedColor : null,
            screenData : 0,
            dialogVisible : false,
            switchValue : true,
            response : null,
            buttCount : {
                min : 0,
                max : 0
            },
            showDialog : true
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true,});
        this._getContext(this.state.token).then(() => {
          this.setState({refreshing: false,});
        });
      }

    _handleTabSelect = async (index)=>{
        this.setState({ active: !this.state.active })
        if(index ==1){
            this.setState({ mapLoading : true, tab : 1, selectedColor : '#ff9933'})
            this._draw(this.state.userLocation, this.state.nearest);
        }else if(index == 2){
            this.setState({ mapLoading : true, tab : 2, selectedColor : '#d9593b'})
            this._draw2(this.state.userLocation, this.state.nearest);
        }else{
            this.setState({ mapLoading : true, tab : 3,  selectedColor : '#a52a2a'})
            this._draw3(this.state.userLocation, this.state.nearest);            
        }
    }
    _retrieveToken = async () =>{
        try{
          var prevToken =  await AsyncStorage.getItem('ubi_token');
          console.log('Token retrieved.');
          if(prevToken != null){
            this._isMounted ? (
              this.setState({
                token : prevToken
              })
            ) : (
              null
            )
          }else{
            console.log('empty')
            this._newToken();
          }
        }catch(exception){
          console.log(exception)
        }
    }
    _getData(token){
        API._status_Request(token, (data) => {
          try{
            var status = data
            if(status == 403){
              console.log('403')
              this._updateToken();
            }else if(status == 200){
              this._getContext(token);
            }else{
              console.log(data);
            }
          }catch(exception){
            console.log(exception);
          }
        })
    }
    _updateToken(){
        this._newToken();
        this._retrieveToken();
        this._getData(this.state.token);
        console.log('Token updated.');
    }
    _newToken(){
        var param = "A1E-d00d1f97994d1be7a533261371bdc28dd2e7"; //api-key
        API._get_Token(param, (data) => {
          try{
              console.log(data);
              this._storeToken(data.token);
          }catch(exception){
              console.log(exception);
          }
        });
    }
    _storeToken = async (newToken) =>{
        try{
          await AsyncStorage.setItem('ubi_token', newToken);
          this._isMounted ? (
            this.setState({
              token : newToken
            })
          ) : (
            null
          )
          console.log('Successfully stored.');
        }catch(exception){
          console.log(exception);
        }
    }
    
    _getContext = async (token) =>{
        // let _API_URL = "https://things.ubidots.com/api/v1.6/";
        // try {
        //     let response = await fetch (_API_URL+ 'devices/bcddc2826bbe/carbon-monoxide-device1/values/?page_size=1',{
        //         headers : {
        //             'X-Auth-Token' : token
        //         }
        //     })
        //     let responseJson = await response.json()
        //     let context1 ={...this.state.context1};
        //     context1.latitude = parseFloat(responseJson.results[0].context.lat);
        //     context1.longitude = parseFloat(responseJson.results[0].context.lng);
        //     this.setState({context1})
        // }catch(error){
        //     console.error(error)
        // }

        // try {
        //     let response = await fetch (_API_URL+ 'devices/b4e62d27-fc1/carbon-monoxide-device2/values/?page_size=1',{
        //         headers : {
        //             'X-Auth-Token' : token
        //         }
        //     })
        //     let responseJson = await response.json()
        //     let context2 ={...this.state.context2};
        //     context2.latitude = parseFloat(responseJson.results[0].context.lat);
        //     context2.longitude = parseFloat(responseJson.results[0].context.lng);
        //     this.setState({context2})

        // }catch(error){
        //     console.error(error)
        // }
        this._getNearestDistance(this.state.token, this.state.userLocation, this.state.context1, this.state.context2);
    }

    _getLocationAsync = async () => {
    const { status } = await Expo.Permissions.askAsync(Expo.Permissions.LOCATION);

        if (status === 'granted') {
            const userLocation = await Expo.Location.getCurrentPositionAsync({
                enableHighAccuracy: true,
            });
        
            this.setState({ 
                userLocation : {
                    ...this.state.userLocation,
                    latitude : userLocation.coords.latitude,
                    longitude : userLocation.coords.longitude
                },
                userMarker : {
                    ...this.state.userMarker,
                    latitude : this.state.userLocation.latitude +','+this.state.userLocation.longitude
                },  
            });

        }
    
    };
    _call_Device1_requests = async (token) =>{
        //last values
         API._get_device1_Last_Value_Monoxide(token, (data) => { //get the latest value from api --carbon monoxide
             try{
                 console.log(data)
                this._isMounted ? (
                     this.setState({
                         lastValueCO : data,
                         loading : false
                     }) 
                ):(
                    null
                )
             }catch(exception){
                 console.log(exception)
             }
         })
         API._get_device1_Last_Value_Smoke(token, (data) => {
            try{
                // console.log(data)
               this._isMounted ? (
                this.setState({
                    lastValueSmoke : data,
                    loading : false
                    })
               ):(
                   null
               )
            }catch(exception){
                console.log(exception)
            }
        })
        API._get_device1_crowdedness(token, (data)=>{
            try{
                console.log(data)
                this.setState({
                    crowd : data,
                    loading : false
                })
            }catch(exception){
                console.log(exception)
            }
        })

     }
     _call_Device2_requests= async (token) =>{ //same rani diri. kani sa isa ka device
           //   //last values
         API._get_device2_Last_Value_Monoxide(token, (data) => {
             try{
                console.log(data)
                 this._isMounted ? (
                     this.setState({
                         lastValueCO : data,
                         loading : false
                     })
                 ):(
                     null
                 )
             }catch(exception){
             console.log(exception)
             }
         })
         API._get_device2_Last_Value_Smoke(token, (data) => {
            try{
                // console.log(data)
                this._isMounted ? (
                    this.setState({
                        lastValueSmoke : data,
                        loading : false
                    })
                ):(
                    null
                )
            }catch(exception){
            console.log(exception)
            }
        })
        API._get_device2_crowdedness(token, (data) =>{
            try{
                console.log(data)
                this.setState({
                    crowd : data,
                    loading : false
                })
            }catch(exception){
                console.log(exception)
            }
        })

     }
    _draw(userLoc, deviceLoc){
        console.log(userLoc)
        console.log(deviceLoc)
        var userlocation = userLoc.latitude+','+userLoc.longitude;
        var devicelocation = deviceLoc.latitude+','+deviceLoc.longitude;
        API._get_Route_Info(userlocation, devicelocation, (data) => {
            try{
                let decoded = Polyline.decode(data.paths[0].points);
                let distance = data.paths[0].distance;
                let time = data.paths[0].time;
                this.setState({
                    time : parseFloat((time/1000)/60).toFixed(2),
                    distance : parseFloat(distance /= 1000).toFixed(1),
                })
                let coordinates = decoded.map((point) => {
                    return{
                        latitude : point[0],
                        longitude : point[1]
                    }
                });
                this.setState({ 
                    lineCoordinates : coordinates,
                    mapLoading : false,
                    loading : false,
                })
            }catch(exception){
                console.log(exception);
            }
        })
    }
    _draw2(userLoc, deviceLoc){

        var userlocation = userLoc.latitude+','+userLoc.longitude;
        var devicelocation = deviceLoc.latitude+','+deviceLoc.longitude;
        API._getBikeInfo(userlocation, devicelocation, (data) => {
            try{
                let decoded = Polyline.decode(data.paths[0].points);
                let distance = data.paths[0].distance;
                let time = data.paths[0].time;
                this.setState({
                    time : parseFloat((time/1000)/60).toFixed(2),
                    distance : parseFloat(distance /= 1000).toFixed(1)
                })
                let coordinates = decoded.map((point) => {
                    return{
                        latitude : point[0],
                        longitude : point[1]
                    }
                });
                this.setState({ 
                    lineCoordinates : coordinates,
                    loading : false,
                    mapLoading : false,
                })
                console.log(this.state.time)
            }catch(exception){
                console.log(exception);
            }
        })
    }
    _draw3(userLoc, deviceLoc){
        var userlocation = userLoc.latitude+','+userLoc.longitude;
        var devicelocation = deviceLoc.latitude+','+deviceLoc.longitude;
        API._getFootInfo(userlocation, devicelocation, (data) => {
            try{
                let decoded = Polyline.decode(data.paths[0].points);
                let distance = data.paths[0].distance;
                let time = data.paths[0].time;
                this.setState({
                    time : parseFloat((time/1000)/60).toFixed(2),
                    distance : parseFloat(distance /= 1000).toFixed(1)
                })
                let coordinates = decoded.map((point) => {
                    return{
                        latitude : point[0],
                        longitude : point[1]
                    }
                });
                this.setState({ 
                    lineCoordinates : coordinates,
                    mapLoading : false,
                    loading : false
                })
                console.log(this.state.time)
            }catch(exception){
                console.log(exception);
            }
        })
    }
    _getNearestDistance(receivedToken, userLoc, deviceLoc1, deviceLoc2){
        var userlocation = userLoc.latitude+','+userLoc.longitude;
        var devicelocation1 = deviceLoc1.latitude+','+deviceLoc1.longitude;
        var devicelocation2 = deviceLoc2.latitude+','+deviceLoc2.longitude;
        var tokenPass = receivedToken;
        var time = 5000;
        this.timer = setInterval( () => 
        API._getNearestContext(tokenPass, userlocation, devicelocation1, devicelocation2, (data) =>{
            try{
                console.log("Data is: " + data)
                // console.log('Screen data is: ' + this.state.screenData)
                if(this.state.screenData == 0){
                    if(data == 1){
                        this.setState({
                            screenData : 1,
                            nearest : {
                                ...this.state.nearest,
                                latitude : deviceLoc1.latitude,
                                longitude : deviceLoc1.longitude
                            }, 
                            name : 'Abreeza',
                            loading : false
                        }, () => this._draw(userLoc, this.state.nearest))
                    }else if(data == 2){
                        this.setState({
                            screenData : 2,
                            nearest : {
                                ...this.state.nearest,
                                latitude : deviceLoc2.latitude,
                                longitude : deviceLoc2.longitude
                            }, 
                            name : 'Robinsons Cybergate',
                            loading : false
                        }, () => this._draw(userLoc, this.state.nearest))
                    }else{
                        console.log(data)
                    }                    
                }else if(this.state.screenData == 1 && data == 2 && this.state.showDialog){
                    this.setState({ dialogVisible : true }, () => {
                        if(this.state.response == true){
                            clearInterval(this.timer)
                            this.setState({
                                screenData : 2,
                                nearest : {
                                    ...this.state.nearest,
                                    latitude : deviceLoc2.latitude,
                                    longitude : deviceLoc2.longitude
                                }, 
                                name : 'Robinsons Cybergate',
                                loading : false,
                                dialogVisible : false,
                                response : false
                            }, () => this._draw(userLoc, this.state.nearest))
                            this._onRefresh()
                        }
                    })
                }else if(this.state.screenData == 2 && data == 1 && this.state.showDialog){
                    this.setState({ dialogVisible : true}, () => {
                        if(this.state.response == true){
                            clearInterval(this.timer)
                            this.setState({
                                screenData : 1,
                                nearest : {
                                    ...this.state.nearest,
                                    latitude : deviceLoc1.latitude,
                                    longitude : deviceLoc1.longitude
                                }, 
                                name : 'Abreeza',
                                loading : false,
                                dialogVisible : false,
                                response : false
                            },  () => this._draw(userLoc, this.state.nearest))
                            this._onRefresh()
                        }
                    })
                }else{
                    null
                }
                this.state.name == 'Abreeza' ? this._call_Device1_requests(this.state.token):this._call_Device2_requests(this.state.token)
                if(this.state.crowd >= 19){
                    this.setState({ isOverCrowded : true })
                }else{
                    this.setState({ isOverCrowded : false })
                }
            }catch(exception){
                console.log(exception);
            }
            // console.log(userlocation)
        }),
        time)
        
    }
    _handleCancel(){
        this.setState({ 
            response: false, 
            dialogVisible : false, 
            showDialog : false, 
        })
        this.timer = setTimeout(() => this.setState({ showDialog : true }), 60000)
    }
    _handleMapPress(){
        this.setState({ active : !this.state.active}, () =>{
            this.props.navigation.navigate('Location', {
                userLoc : this.state.userLocation,
                deviceLoc : this.state.nearest,
                coordinates : this.state.lineCoordinates,
                color : this.state.selectedColor
            });
        })
    }
    _handleStats(){
        this.props.navigation.navigate('Statistics', {
            token : this.state.token
        });
    }
    componentWillMount(){
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
              errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
            this._getLocationAsync();
        }
 
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted ? (
            this._newToken(),
            this._retrieveToken(),
            this._getData(this.state.token)
        ):(
            null
        )

    }
    componentWillUnmount(){
        this._isMounted = false;
        clearInterval(this.timer);
        this.setState({
          loading : true
        })
    }
    
  
    render(){
        let results;
        let tab;
        let map;
        let fab;
        let dialog;
        if(this.state.loading){
            results = (
                <Loader />
            )
        }else{
            results = (
                <Content>
                    <ReportsScreen name={this.state.name}  distance={this.state.distance} time={this.state.time} smoke={this.state.lastValueSmoke} crowd={this.state.isOverCrowded} buttCountMax={this.state.buttCount.max} buttCountMin={this.state.buttCount.min} co={this.state.lastValueCO} lastToken = {this.state.token}/>
                </Content>
            )
            {this.state.tab == 1?(
                tab =(<VehicleTab tab={1} onPress={(tab) => this._handleTabSelect(tab)}/>) 
            ):this.state.tab == 2?(
                tab = ( <VehicleTab tab={2} onPress={(tab) => this._handleTabSelect(tab)}/>)
            ):(
                tab=(<VehicleTab tab={3} onPress={(tab) => this._handleTabSelect(tab)}/>)
            )}
            fab = (
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => this.setState({ active: !this.state.active })}>
                <Icon type="Feather" name="map" style={{color : 'white'}} size={20} />
                <Button style={{ backgroundColor: 'green', color: 'white'}} onPress={() => this._handleStats()}>
                    <Icon type={"Feather"} name="activity" />
                </Button>
                <Button style={{ backgroundColor: '#EAA936', color: 'white'}} size={20} onPress={() => this._handleMapPress()}>
                    <Icon type="Feather" name="zoom-in" />
                </Button>
                <Button style={{ backgroundColor: '#ff9933', color: 'white'}} onPress={() => this._handleTabSelect(1)}>
                    <Icon type={"Ionicons"} name="md-car" />
                </Button>
                <Button style={{ backgroundColor: '#d9593b', color: 'white'}} onPress={() => this._handleTabSelect(2)}>
                    <Icon type={"Ionicons"} name="md-bicycle" />
                </Button>
                <Button style={{ backgroundColor: '#a52a2a', color: 'white'}} onPress={() => this._handleTabSelect(3)}>
                    <Icon type={"Ionicons"} name="md-walk" />
                </Button>
              </Fab>
            )
            if( this.state.showDialog ){
                dialog = (
                    <ConfirmDialog
                    title="Heads up!"
                    message="A better DSA is found. Would you like to take this route instead?"
                    visible={this.state.dialogVisible}
                    onTouchOutside={null}
                    positiveButton={{
                        title: "YES",
                        onPress: () => this.setState({ response : true})
                    }}
                    negativeButton={{
                        title: "NO",
                        onPress: () => this._handleCancel()
                    }}
                />
                )
            }else{
                dialog = (null);
            }
        }
        return(
            <Container>
                    <ScrollView style={{flex : 1}} refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} colors={['#EAA936']}/>
        }>                          
                        <Content contentContainerStyle={{flex : 1, position : 'relative', height : 300}}>
                            {this.state.userLocation.latitude != 0.0 && this.state.userLocation.longitude != 0.0 && this.state.lineCoordinates != null && !this.state.mapLoading ?(
                                <LocationMap userLoc={this.state.userLocation} coordinates = {this.state.lineCoordinates} deviceLoc = {this.state.nearest} />
                            ):(
                                <Text style={{justifyContent : 'center', alignSelf :'center', marginTop : 150}}>Loading map...</Text>
                            )}
                        </Content>
                                {results}
                    </ScrollView>
                    {fab}
                    {dialog}
                    {/* {tab} */}
            </Container>
        );
    }
}

export default InformationScreen;
