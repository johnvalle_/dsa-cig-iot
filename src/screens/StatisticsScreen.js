import React from 'react';
import { Content, Container, Card, CardItem, Body, Icon, FooterTab, Button, List, ListItem, Separator, Right} from "native-base";
import API from '../api/initalize';
import { View, Dimensions, Text, ScrollView, Image, } from 'react-native';
import moment from 'moment';
import Loader from '../components/Loader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph
  } from 'react-native-chart-kit'

class StatisticsScreen extends React.Component{
    _isMounted = true;

    static navigationOptions = ({ navigation }) => ({
        title: 'DSA-Cig-IoT',
        headerBackTitle: null,
        headerTitleStyle:{
            alignSelf:'center',
            textAlign: 'center',
            width: wp('65%'),
        },
        headerStyle: {
            backgroundColor: '#ffc400',
        },
        headerTintColor: '#000000',
    });
 
    constructor(props){
        super(props);
        this.state = {
            prevDays : [],
            timeFrom : [],
            timeTo : [],
            mean : [],
            mean2 : [],
            loading : true,
            token : null
        }
    }
    _getPrevDays(){
        var val = 7;
        var arr_from = [];
        var arr_to = [];

        for( var x = 0; x<=6; x++){
            let date = moment();
            var prev = date.subtract(7, 'days');
            let prevDay = prev.startOf('isoWeek');
            let prevWeekDay = prevDay.add((x), 'days').valueOf().toString();
            arr_from.push(prevWeekDay);   
        }
        for( var x = 0; x<=6; x++){
            let date = moment();
            var prev = date.subtract(7, 'days');
            let prevDay = prev.startOf('isoWeek');
            let end = prevDay.endOf('day');
            let prevWeekDay = end.add((x), 'days').valueOf().toString();
            arr_to.push(prevWeekDay);
        }
        this.setState({
            timeFrom : arr_from,
            timeTo : arr_to
        }, () => this._getMean())
    
    }
    _getMean(){
        var counter = 0;
        this._getMeanHandler(counter);
        this._getMeanHandler2(counter);
    }
    _display(data){
        console.log(data)
    }
    _getMeanHandler(counter){
        if(counter != this.state.timeFrom.length){
            API._getMeanDev1(this.state.token, this.state.timeFrom[counter], this.state.timeTo[counter], (data) =>{
                try{
                    if(data == " " || data == null || data==""){
                        this._getMeanHandler(counter)
                    }else{
                        var x = data;
                        console.log(data)
                        var y = x.substring(x.lastIndexOf(':')+1)
                        y = x.substring(x.lastIndexOf(' ')+1)
                        var z = y.slice(0, -1);
                        this.setState({
                            mean : [...this.state.mean, parseFloat(z)]
                        })
                        this._getMeanHandler(counter+1);
                    }
                }catch(exception){
                    console.log(exception)
                }
            })
        }else{
            for(var x = 0; x<=this.state.mean.length; x++){
                console.log(x+": "+this.state.mean[x])
            }
            this.setState({
                loading : false
            })
        }
    }
    _getMeanHandler2(counter){
        if(counter != this.state.timeFrom.length){
            API._getMeanDev2(this.state.token, this.state.timeFrom[counter], this.state.timeTo[counter], (data) =>{
                try{
                    if(data == " " || data == null || data==""){
                        this._getMeanHandler2(counter)
                    }else{
                        var x = data;
                        console.log(data)
                        var y = x.substring(x.lastIndexOf(':')+1)
                        y = x.substring(x.lastIndexOf(' ')+1)
                        var z = y.slice(0, -1);
                        this.setState({
                            mean2 : [...this.state.mean2, parseFloat(z)]
                        })
                        this._getMeanHandler2(counter+1);
                    }
                }catch(exception){
                    console.log(exception)
                }
            })
        }else{
            for(var x = 0; x<=this.state.mean.length; x++){
                console.log(x+": "+this.state.mean2[x])
            }
            this.setState({
                loading : false
            })
        }
    }
    componentDidMount(){
        const { navigation } = this.props;
        const token = navigation.getParam('token');
        console.log(token)
        if(this._isMounted){
            
            this.setState({
                token : token
            }, () =>  this._getPrevDays())
        }
    }
    componentWillUnmount(){
        _isMounted = false;
    }
    render(){
        let res;
        let list;
        this.state.loading ? (
            res = (
                <Content style={{marginTop : 300}}>
                     <Loader/>
                </Content>
            )
        ):(
            res = (
                <React.Fragment>
                    <View>
                        <Text style={{textAlign : 'center', padding : 20}}>
                            Abreeza Average Carbon Monoxide levels
                        </Text>
                        <LineChart
                            data={{
                                labels: ['Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                                datasets: [{ data: this.state.mean }]
                            }}
                            width={(Dimensions.get('window').width)*.9} // from react-native
                            height={220}
                            chartConfig={{
                                backgroundColor: '#f7f7f7',
                                backgroundGradientFrom: '#f9f9f9',
                                backgroundGradientTo: '#efefef',
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                    style: {
                                        borderRadius: 16
                                    }
                            }}
                            bezier
                            style={{
                                marginVertical: 8,
                                borderRadius: 16,
                                elevation :4
                            }}
                        />
                        <Text style={{textAlign : 'center', padding : 20}}>
                            Robinsons Cybergate Average Carbon Monoxide levels
                        </Text>
                        <LineChart
                            data={{
                                labels: ['Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                                datasets: [{ data: this.state.mean2 }]
                            }}
                            width={(Dimensions.get('window').width)*.9} // from react-native
                            height={220}
                            chartConfig={{
                                backgroundColor: '#f7f7f7',
                                backgroundGradientFrom: '#f9f9f9',
                                backgroundGradientTo: '#efefef',
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                    style: {
                                        borderRadius: 16
                                    }
                            }}
                            bezier
                            style={{
                                marginVertical: 8,
                                borderRadius: 16,
                                elevation : 4
                            }}
                        />
                    </View>
                </React.Fragment>
            ),
            list = (
                <React.Fragment>
                     <View>
                        <Image
                            style={{width : (Dimensions.get('window').width)*.9, height: 200, marginTop : 20, borderRadius : 20}}
                            source={require('../../assets/aqi.png')}
                        />
                    </View>
                    <Container style={{marginTop : 30}}>
                        <Content>
                            <Separator bordered>
                                <Text>Abreeza</Text>
                            </Separator>
                            {this.state.mean.map((mean_data, index) => 
                                <ListItem noIndent key={index}>
                                    {/* <Text style={{color: 'black'}}>{mean_data}</Text> */}
                                    <Body>
                                        <Text style={{color: 'black'}}>
                                            {
                                                index == 0? "Monday" 
                                                : index == 1? "Tuesday" 
                                                : index == 2? "Wednesday" 
                                                : index ==3 ?"Thursday" 
                                                : index ==4 ? "Friday" 
                                                : index == 5? "Saturday" 
                                                : index == 6? "Sunday" 
                                                : null 
                                            }
                                        </Text>
                                    </Body>
                                    <Right>
                                        <Icon type="FontAwesome" name="circle" size={30} style={ 
                                                mean_data >= 0 && mean_data <= 50 ? {color : '#00E043'} 
                                                : mean_data >= 51 && mean_data <= 100 ? {color : '#EAA936'} 
                                                : mean_data >= 101 && mean_data <=150? {color : '#ff9933'} 
                                                : mean_data >= 151 && mean_data <= 200 ? {color:'#a52a2a'} 
                                                : mean_data >= 201 && mean_data <= 300 ? {color : '#5067FF'} 
                                                : null
                                            }
                                        />
                                    </Right>
                                </ListItem>
                            )}
                            <Separator bordered>
                                <Text>Robinsons Cybergate</Text>
                            </Separator>
                            {this.state.mean2.map((mean_data, index) => 
                            <ListItem noIndent key={index}>
                                {/* <Text style={{color: 'black'}}>{mean_data}</Text> */}
                                <Body>
                                    <Text style={{color: 'black'}}>
                                        {
                                            index == 0? "Monday" 
                                            : index == 1? "Tuesday" 
                                            : index == 2? "Wednesday" 
                                            : index ==3 ?"Thursday" 
                                            : index ==4 ? "Friday" 
                                            : index == 5? "Saturday" 
                                            : index == 6? "Sunday" 
                                            : null 
                                        }
                                    </Text>
                                </Body>
                                <Right>
                                    <Icon type="FontAwesome" name="circle" size={30} style={ 
                                            mean_data >= 0 && mean_data <= 50 ? {color : '#00E043'} 
                                            : mean_data >= 51 && mean_data <= 100 ? {color : '#EAA936'} 
                                            : mean_data >= 101 && mean_data <=150? {color : '#ff9933'} 
                                            : mean_data >= 151 && mean_data <= 200 ? {color:'#a52a2a'} 
                                            : mean_data >= 201 && mean_data <= 300 ? {color : '#5067FF'} 
                                            : null
                                        }
                                    />
                                </Right>
                            </ListItem>
                            )}
                        </Content>
                    </Container>
                </React.Fragment>
            )
        )
        return (
            <Container style={{justifyContent: 'center', alignItems:'center'}}>
                <ScrollView>
                    {res}
                    {list}
                </ScrollView>                
            </Container>
        );
    }
}

export default StatisticsScreen;