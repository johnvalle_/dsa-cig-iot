import React from 'react';

class Endpoints extends React.Component{
    _API_URL = "https://things.ubidots.com/api/v1.6/";
    _API_KEY_GH = "e9b419e9-67aa-4c90-9d2c-189f0910b29a";
    // this function is a direct api request
    _API_GET = async (source, token, callback) =>{
        try {
            let response = await fetch (source,{
                headers : {
                    'X-Auth-Token' : token
                }
            })
            let responseJson = await response.text()
            return callback(responseJson)
        }catch(error){
            console.error(error)
        }
    }
    // requests for session token
    _get_Token = async (param, callback) => {
        var source = "https://things.ubidots.com/api/v1.6/auth/token/";
        try {
            let response = await fetch (source,{
                method : 'POST',
                headers : {
                    'x-ubidots-apikey' : param
                }
            })
            let responseJson = await response.json()
            return callback(responseJson)
        }catch(error){
            console.error(error)
        }
    }
    // a mock api request to check if token is valid through status codes
    _status_Request = async (token, callback) => {
        var source = this._API_URL+ 'devices/bcddc2826bbe/carbon-monoxide-device1/lv/';
        try {
            let response = await fetch (source,{
                headers : {
                    'X-Auth-Token' : token
                }
            })
            let responseStatus = await response.status
            return callback(responseStatus)
        }catch(error){
            console.error(error)
        }
    }
    // device 1 requests
    _get_device1_Last_Value_Monoxide = async (param, callback) => {

        this._API_GET(
            this._API_URL + 'devices/bcddc2826bbe/carbon-monoxide-device1/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device1_Last_Value_Monoxide(param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }

    _get_device1_Last_Value_Smoke = async (param, callback) => {

        this._API_GET(
            this._API_URL + 'devices/bcddc2826bbe/smoke-device1/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device1_Last_Value_Smoke (param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }

    //device 2 requests
    _get_device2_Last_Value_Monoxide = async (param, callback) => {

        this._API_GET(
            this._API_URL + 'devices/b4e62d27-fc1/carbon-monoxide-device2/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device2_Last_Value_Monoxide(param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }

    _get_device2_Last_Value_Smoke = async (param, callback) => {

        this._API_GET(
            this._API_URL + 'devices/b4e62d27-fc1/smoke-device2/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device2_Last_Value_Smoke (param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }

    _get_Route_Info = async (userLoc, deviceLoc, callback) => {
        try{
            let response = await fetch ('https://graphhopper.com/api/1/route?point='+userLoc+'&point='+deviceLoc+'&vehicle=car&locale=en&calc_points=true&points_encoded=true&key='+this._API_KEY_GH);
            let responseJson = await response.json();
            return callback(responseJson);
            // return callback('https://graphhopper.com/api/1/route?point='+userLoc+'&point='+deviceLoc+'&vehicle=car&locale=en&points_encoded=false&key='+API_key);
        }catch(error){
            console.error(error);
        }
    }
    
    _getFootInfo = async (userLoc, deviceLoc, callback) => {
        try{
            let response = await fetch ('https://graphhopper.com/api/1/route?point='+userLoc+'&point='+deviceLoc+'&vehicle=foot&locale=en&calc_points=true&points_encoded=true&key='+this._API_KEY_GH);
            let responseJson = await response.json();
            return callback(responseJson);
        }catch(error){
            console.error(error);
        }
    }
    _getBikeInfo = async (userLoc, deviceLoc, callback) => {
        try{
            let response = await fetch ('https://graphhopper.com/api/1/route?point='+userLoc+'&point='+deviceLoc+'&vehicle=bike&locale=en&calc_points=true&points_encoded=true&key='+this._API_KEY_GH);
            let responseJson = await response.json();
            return callback(responseJson);
        }catch(error){
            console.error(error);
        }
    }
    _get_device1_crowdedness = async (param, callback) =>{
        this._API_GET(
            this._API_URL + 'devices/bcddc2826bbe/crowdednessperfive/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device1_crowdedness(param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }
    _get_device2_crowdedness = async (param, callback) =>{
        this._API_GET(
            this._API_URL + 'devices/b4e62d27-fc1/crowdednessperfive/lv/',
            param,
            (data) => {
                if(data == null || data == "" || data == " "){
                    this._get_device2_crowdedness(param, callback)
                }else{
                    callback(data);
                }
            }
        );
    }
    _getNearestContext = async (token, user, device1, device2, callback) => {
        let distance1;
        let distance2;
        let result1;
        let result2;
        let nearest;
        this._get_Route_Info(user, device1, (data)=>{
                distance1 = parseFloat(data.paths[0].distance);

                this._get_Route_Info(user, device2, (data)=>{
                distance2 = parseFloat(data.paths[0].distance);  

                if(distance1 < distance2){
                    nearest = 1
                }else{
                    nearest = 2
                }
                this._get_device1_crowdedness(token, (data) =>{
                    if(data>=19){
                        result1 = true;
                    }else{
                        result1 = false;
                    }
                    console.log("data1: "+ data)
                    this._get_device2_crowdedness(token, (data) =>{
                        if(data>=19){
                            result2 = true;
                        }else{
                            result2 = false;
                        }
                        console.log("data2: "+ data)
                        if(result1){
                            if(result2){
                                callback(nearest)
                            }else{
                                callback(2)
                            }
                        }else if(result2){
                            if(result1){
                                callback(nearest)
                            }else{
                                callback(1)
                            }
                        }else if(!result1 && !result2){
                            callback(nearest)
                        }else{
                            null
                        }
                    })
                })
            })
        });        
    }
    
    
    _getMeanDev1 = async(param, from, to, callback) =>{
        this._API_GET(
            this._API_URL+'variables/5c6020f35916365cd2edaa29/statistics/mean/'+from+'/'+to+'/',
            param,
            (data) =>{
                callback(data)
            }
        )
    }   
    _getMeanDev2 = async(param, from, to, callback) =>{
        this._API_GET(
            this._API_URL+'variables/5c6021d35916365cd2edb03a/statistics/mean/'+from+'/'+to+'/',
            param,
            (data) =>{
                callback(data)
            }
        )
    }    
}

const API = new Endpoints();
export default API;

