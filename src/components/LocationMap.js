import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Constants, MapView } from 'expo';
import Loader from "../components/Loader";

class LocationScreen extends Component {

    _isMounted = false;
    state = {
    userMarker : {
        title : "It's you, user.",
        description : 'Your current location.'
        },
        deviceMarker : {
        title : "DSA",
        description : 'The DSA is located here.'
        },
        errorMessage: null,
        isMapReady : false,
        coordinates : null,
    };

    onMapLayout = () => {
        this.setState({
        isMapReady : true
        })
    }

    render() {
        let result;
        if(this.state.loading){
        result = <Loader />
        }else{
        result = (
         
            <View style={{flex : 1}}>
                <MapView
                style={styles.map}
                initialRegion={{
                    latitude: this.props.userLoc.latitude,
                    longitude: this.props.userLoc.longitude,
                    latitudeDelta: 0.0043,
                    longitudeDelta: 0.0034,
                }}
                onLayout={this.onMapLayout}
                >
                {
                this.state.isMapReady && 
                    <MapView.Marker
                    coordinate={ this.props.userLoc}
                    title={this.state.userMarker.title}
                    description={this.state.userMarker.description}
                    />
                } 
                {
                this.state.isMapReady && 
                    <MapView.Marker
                    coordinate={ this.props.deviceLoc}
                    title={this.state.deviceMarker.title}
                    description={this.state.deviceMarker.description}
                    pinColor={'green'}
                    />
                }
                {
                    this.props.coordinates &&
                    <MapView.Polyline
                    coordinates = {this.props.coordinates}
                    strokeWidth = {4}
                    strokeColor = {"#EAA936"}
                    />
                }
                </MapView>
            </View>
        )
      
    }
    return (
      result
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
  map :{
    ...StyleSheet.absoluteFillObject
  }
});
 
export default LocationScreen;

