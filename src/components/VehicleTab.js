import React from 'React';
import {Text, Button, Footer, FooterTab, Content, Container, Header, Icon } from 'native-base';

class VehicleTab extends React.Component{
    _handlePress(index){
      this.props.onPress(index);
    }
    render() {
      let tab = this.props.tab;
      let buttonGroup;
      if(tab==1){
        buttonGroup = (
          <FooterTab>
            <Button onPress={() => this._handlePress(1)} active>
              <Icon type={"Ionicons"} name="md-car" />
            </Button>
            <Button onPress={() => this._handlePress(2)}>
              <Icon type={"Ionicons"} name="md-bicycle" />
            </Button>
            <Button onPress={() => this._handlePress(3)}>
              <Icon type={"Ionicons"} name="md-walk" />
            </Button>
          </FooterTab>
        );
      }else if(tab ==2){
        buttonGroup = (
          <FooterTab>
            <Button onPress={() => this._handlePress(1)}>
              <Icon type={"Ionicons"} name="md-car" />
            </Button>
            <Button onPress={() => this._handlePress(2)} active>
              <Icon type={"Ionicons"} name="md-bicycle" />
            </Button>
            <Button onPress={() => this._handlePress(3)}>
              <Icon type={"Ionicons"} name="md-walk" />
            </Button>
          </FooterTab>
        );
      }else{
        buttonGroup = (
          <FooterTab>
            <Button onPress={() => this._handlePress(1)}>
              <Icon type={"Ionicons"} name="md-car" />
            </Button>
            <Button onPress={() => this._handlePress(2)}>
              <Icon type={"Ionicons"} name="md-bicycle" />
            </Button>
            <Button onPress={() => this._handlePress(3)} active>
              <Icon type={"Ionicons"} name="md-walk" />
            </Button>
          </FooterTab>
        );
      }
      return (
          <Footer>
            {buttonGroup}
          </Footer>
      );
      }
}

export default VehicleTab;