import React from "react";
import { ActivityIndicator } from "react-native";

const Loader = () => {
  return (
    <ActivityIndicator
      size="large"
      color="#eaa936"
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    />
  );
};

export default Loader;
