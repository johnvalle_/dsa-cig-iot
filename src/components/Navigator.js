import  HomeScreen  from "../screens/HomeScreen";
import  LocationScreen from "../screens/LocationScreen";
import { createStackNavigator, createAppContainer } from "react-navigation";

const AppStackNavigator = createStackNavigator({
  Home: { screen: HomeScreen,  navigationOptions: { gesturesEnabled: false }  },
  Location : {screen: LocationScreen,  navigationOptions: { gesturesEnabled: false } },
  headerMode: 'screen',
  }, {
    initialRouteName: 'Location'
  });

const Navigator = createAppContainer(AppStackNavigator);

export default Navigator;

