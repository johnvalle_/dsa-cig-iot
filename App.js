import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation'

import LocationScreen from './src/screens/LocationScreen';
import InformationScreen from './src/screens/InformationScreen';
import StatisticsScreen from './src/screens/StatisticsScreen';

const MainNavigator = createStackNavigator({

  //MAIN PAGES
  Location: { screen: LocationScreen, navigationOptions: { gesturesEnabled: false } },
  Information: { screen: InformationScreen, navigationOptions: { gesturesEnabled: false } },
  Statistics: { screen: StatisticsScreen, navigationOptions: { gesturesEnabled: false } },
  headerMode: 'screen',
  }, {
    initialRouteName: 'Information'
  });

const App = createAppContainer(MainNavigator);

export default App;
